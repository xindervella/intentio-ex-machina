SEIntentFirewall
================

Wiki
----

If you're looking for guides on how to use IEM, please visit the [wiki](https://bitbucket.org/carter-yagemann/intentio-ex-machina/wiki/).

Branch
------

This is the master branch. This contains the most generic copy of the IEM code.